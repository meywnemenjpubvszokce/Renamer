import PyQt5
import sys
import layout
import os


class MainWindow(PyQt5.QtWidgets.QDialog, layout.Ui_Form):
    def __init__(self):
        self.frontText = ''
        self.behindText = ''

        PyQt5.QtWidgets.QDialog.__init__(self)

        self.setupUi(self)
        
        self.openDirectory.clicked.connect(self.addOriginalFilesToTable)
        self.frontInsertTextButton.clicked.connect(self.setFrontText)
        self.behindInsertTextButton.clicked.connect(self.setBehindText)
        self.frontInsertNumberButton.clicked.connect(self.setFrontNumber)
        self.behindInsertNumberButton.clicked.connect(self.setBehindNumber)
        self.deleteTextButton.clicked.connect(self.deleteTextFromNames)
        self.replaceTextButton.clicked.connect(self.replaceTextFromNames)
        self.saveButton.clicked.connect(self.saveChanges)
    
    def applyNamedFiles(self):
        for i in range(self.filesLength):
            self.renamedFiles[i] = self.frontText + self.originalFiles[i] + self.behindText
            self.namesTable.setItem(i, 1, PyQt5.QtWidgets.QTableWidgetItem(self.frontText + self.originalFiles[i] + self.behindText))

    def getDirectory(self):
        return PyQt5.QtWidgets.QFileDialog.getExistingDirectory(None, '폴더 선택', '/home/', PyQt5.QtWidgets.QFileDialog.ShowDirsOnly)
    
    def getFilesList(self):
        l = []

        for i in os.listdir(self.rootPath):
            if os.path.isfile(os.path.join(self.rootPath, i)):
                l.append(i)

        return l

    def addOriginalFilesToTable(self):
        try:
            self.rootPath = self.getDirectory()
            files = self.getFilesList()
            files.sort()
            filesName = []
            self.filesExtension = []

            for i in files:
                t = os.path.splitext(i)
                self.filesExtension.append(t[1])
                filesName.append(t[0])

            self.originalFiles = filesName.copy()
            self.renamedFiles = filesName.copy()
            self.filesLength = len(self.originalFiles)

            self.namesTable.setRowCount(len(files))

            for i in range(len(files)):
                self.namesTable.setItem(i, 0, PyQt5.QtWidgets.QTableWidgetItem(filesName[i]))
            
            self.applyNamedFiles()
        except:
            message = PyQt5.QtWidgets.QMessageBox()
            message.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            message.setWindowTitle("Error")
            message.setText("폴더가 선택되지 않았습니다")
            message.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)

            message.exec_()

    def setFrontText(self):
        self.frontText = self.frontInsertText.toPlainText()

        self.applyNamedFiles()
    
    def setBehindText(self):
        self.behindText = self.behindInsertText.toPlainText()

        self.applyNamedFiles()

    def setFrontNumber(self):
        startNumber = self.startNumberSpinBox.value()
        increasement = self.increasementSpinBox.value()

        self.frontText = ''
        n = startNumber

        for i in range(self.filesLength):
            self.renamedFiles[i] = str(n) + self.originalFiles[i] + self.behindText
            self.namesTable.setItem(i, 1, PyQt5.QtWidgets.QTableWidgetItem(str(n) + self.originalFiles[i] + self.behindText))

            n += increasement

    def setBehindNumber(self):
        startNumber = self.startNumberSpinBox.value()
        increasement = self.increasementSpinBox.value()

        self.behindText = ''
        n = startNumber

        for i in range(self.filesLength):
            self.renamedFiles[i] = self.frontText + self.originalFiles[i] + str(n)
            self.namesTable.setItem(i, 1, PyQt5.QtWidgets.QTableWidgetItem(self.frontText + self.originalFiles[i] + str(n)))

            n += increasement
    
    def deleteTextFromNames(self):
        text = self.deleteText.toPlainText()
        
        for i in range(self.filesLength):
            self.renamedFiles[i] = self.renamedFiles[i].replace(text, '')
            self.namesTable.setItem(i, 1, PyQt5.QtWidgets.QTableWidgetItem(self.renamedFiles[i]))


    def replaceTextFromNames(self):
        fromText = self.replaceFromText.toPlainText()
        toText = self.replaceToText.toPlainText()

        for i in range(self.filesLength):
            self.renamedFiles[i] = self.renamedFiles[i].replace(fromText, toText)
            self.namesTable.setItem(i, 1, PyQt5.QtWidgets.QTableWidgetItem(self.renamedFiles[i]))
    
    def saveChanges(self):
        for i in range(self.filesLength):
            os.rename(os.path.join(self.rootPath, self.originalFiles[i]) + self.filesExtension[i],\
                      os.path.join(self.rootPath,  self.renamedFiles[i]) + self.filesExtension[i])


if __name__ == "__main__":
    app = PyQt5.QtWidgets.QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
